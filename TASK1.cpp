#include <iostream>
#include <string>
using std::cout;
using std::cin;

int main()
{
    unsigned int score;

    while (1) {
        cin >> score;
        if (score > 100) cout << "Invalid score";
        else if (score >= 90) cout << "Grade A";
        else if (score >= 80) cout << "Grade B";
        else if (score >= 70) cout << "Grade C";
        else if (score >= 60) cout << "Grade D";
        else if (score >= 0) cout << "Grade F";
    }
    return 0;
}
