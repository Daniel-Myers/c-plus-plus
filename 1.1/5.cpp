#include <iostream>
#include <string>
using std::cout;
using std::endl;

int main()
{
    int result;
    for (int i=1; i<=12; i++) {
        for (int j=1; j<=12; j++) {
            result = i * j;
            cout << i << " * " << j << " = " << result << endl;
        }
        cout << endl;
    }
    return 0;
}
