#include <iostream>
#include <string>
using std::cout;
using std::cin;

int main()
{
    unsigned int n;
    unsigned int total = 0;
    cout << "Enter a number: ";
    cin >> n;
    for (int i=1; i<=n; i++) {
        total += i;
    }
    cout << total;
    return 0;
}
