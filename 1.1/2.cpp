#include <iostream>
#include <string>
using std::cout;
using std::cin;
using std::string;

int main()
{
    std::string name;
    cout << "What is your name? ";
    cin >> name;
    cout << "Hello " << name << "!";
    return 0;
}
