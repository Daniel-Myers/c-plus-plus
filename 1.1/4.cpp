#include <iostream>
#include <string>
using std::cout;
using std::cin;

int main()
{
    unsigned int n;
    unsigned int total = 0;
    cout << "Enter a number: ";
    cin >> n;
    for (int i=1; i<=n; i++) {
        if ((i % 3 == 0) || (i % 5 == 0)) {
            total += i;
        }
    }
    cout << total;
    return 0;
}
