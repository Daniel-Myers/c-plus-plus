#include <iostream>
#include <string>
using std::cout;
using std::endl;

int main()
{
    int result;
    for (int i=2; i<=100; i++) {
        bool prime = true;
        for (int j=2; j<=100; j++) {
            if ((i % j == 0) && (i != j)) {
                prime = false;
                break;
            }
        }
        if (prime) cout << i << endl;
    }
    return 0;
}
