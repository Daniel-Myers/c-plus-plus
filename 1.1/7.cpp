#include <iostream>
#include <string>
using std::cout;
using std::endl;

int main()
{
    int currentYear = 2019;
    int leapYearCout = 0;

    while (leapYearCout < 20) {
        if (currentYear % 4 == 0) {
            if (currentYear % 100 == 0) {
                if (currentYear % 400 == 0) {
                    cout << currentYear << endl;
                    leapYearCout += 1;
                }
            }
            else
                cout << currentYear << endl;
                leapYearCout += 1;
        }
        currentYear += 1;
    }
    return 0;
}
